# Use a imagem base do OpenJDK
FROM openjdk:17-jdk-alpine

# Defina o diretório de trabalho no contêiner
WORKDIR /app

# Copie os arquivos do projeto para o contêiner
COPY target/library.jar /app/library.jar

# Comando para executar o aplicativo quando o contêiner for iniciado
CMD ["java", "-jar", "library.jar"]
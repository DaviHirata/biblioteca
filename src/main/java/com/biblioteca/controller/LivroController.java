package main.java.com.biblioteca.controller;

import main.java.com.biblioteca.model.Livro;
import main.java.com.biblioteca.service.LivroService;

import java.util.List;

public class LivroController {
    private LivroService livroService = new LivroService();

    public Livro salvarLivro(String titulo, String autor, int anoPublicacao) {
        Livro livro = new Livro(titulo, autor, anoPublicacao);
        return livroService.salvarLivro(livro);
    }

    public Livro buscarLivroPorId(int id) {
        return livroService.buscarLivroPorId(id);
    }

    public List<Livro> listarTodosLivros() {
        return livroService.listarTodosLivros();
    }

    public void editarLivro(int id, String novoTitulo, String novoAutor, int novoAnoPublicacao) {
        Livro livro = livroService.buscarLivroPorId(id);
        livro.setTitulo(novoTitulo);
        livro.setAutor(novoAutor);
        livro.setAnoPublicacao(novoAnoPublicacao);
        livroService.editarLivro(livro);
    }

    public void deletarLivro(int id) {
        livroService.deletarLivro(id);
    }
}

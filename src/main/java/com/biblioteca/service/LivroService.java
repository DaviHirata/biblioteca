package main.java.com.biblioteca.service;

import main.java.com.biblioteca.dao.LivroDAO;
import main.java.com.biblioteca.model.Livro;

import java.util.List;

public class LivroService {
    private LivroDAO livroDAO = new LivroDAO();

    public Livro salvarLivro(Livro livro) {
        return livroDAO.salvar(livro);
    }

    public Livro buscarLivroPorId(int id) {
        return livroDAO.buscarPorId(id);
    }

    public List<Livro> listarTodosLivros() {
        return livroDAO.listarTodos();
    }

    public void editarLivro(Livro livro) {
        livroDAO.editar(livro);
    }

    public void deletarLivro(int id) {
        livroDAO.deletar(id);
    }
}
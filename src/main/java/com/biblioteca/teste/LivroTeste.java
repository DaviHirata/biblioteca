package com.biblioteca.teste;

import main.java.com.biblioteca.dao.LivroDAO;
import main.java.com.biblioteca.model.Livro;

public class LivroTeste {

    public static void main(String[] args) {
        // Criação de um livro
        Livro livro = new Livro("Mulheres, Raça e Classe", "Angela Davis", 1981);

        // Salvar o livro na biblioteca
        LivroDAO livroDAO = new LivroDAO();
        Livro livroSalvo = livroDAO.salvar(livro);

        // Exibindo informações sobre o livro salvo
        if (livroSalvo != null) {
            System.out.println("Livro salvo com sucesso!");
            exibirDetalhesLivro(livroSalvo);
        } else {
            System.out.println("Falha ao salvar o livro.");
        }
    }

    private static void exibirDetalhesLivro(Livro livro) {
        System.out.println("ID: " + livro.getId());
        System.out.println("Título: " + livro.getTitulo());
        System.out.println("Autor: " + livro.getAutor());
        System.out.println("Ano de Publicação: " + livro.getAnoPublicacao());
    }
}

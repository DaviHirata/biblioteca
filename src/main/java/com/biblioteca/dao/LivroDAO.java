package main.java.com.biblioteca.dao;

import main.java.com.biblioteca.model.Livro;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LivroDAO {
    private static final String URL = "jdbc:postgresql://localhost:5432/biblioteca";
    private static final String LOGIN = "postgres";
    private static final String SENHA = "1234";

    public Livro salvar(Livro livro) {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, SENHA)) {
            String sql = "INSERT INTO livros (titulo, autor, ano_publicacao) VALUES (?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, livro.getTitulo());
                statement.setString(2, livro.getAutor());
                statement.setInt(3, livro.getAnoPublicacao());
                int affectedRows = statement.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException("Falha ao inserir livro, nenhum registro foi adicionado.");
                }

                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        livro.setId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Falha ao obter o ID gerado para o livro.");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return livro;
    }

    public Livro buscarPorId(int id) {
        Livro livro = null;
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, SENHA)) {
            String sql = "SELECT * FROM livros WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, id);
                try (ResultSet resultSet = statement.executeQuery()) {
                    if (resultSet.next()) {
                        livro = new Livro();
                        livro.setId(resultSet.getInt("id"));
                        livro.setTitulo(resultSet.getString("titulo"));
                        livro.setAutor(resultSet.getString("autor"));
                        livro.setAnoPublicacao(resultSet.getInt("ano_publicacao"));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return livro;
    }

    public List<Livro> listarTodos() {
        List<Livro> livros = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, SENHA)) {
            String sql = "SELECT * FROM livros";
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(sql)) {
                    while (resultSet.next()) {
                        Livro livro = new Livro();
                        livro.setId(resultSet.getInt("id"));
                        livro.setTitulo(resultSet.getString("titulo"));
                        livro.setAutor(resultSet.getString("autor"));
                        livro.setAnoPublicacao(resultSet.getInt("ano_publicacao"));
                        livros.add(livro);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return livros;
    }

    public void editar(Livro livro) {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, SENHA)) {
            String sql = "UPDATE livros SET titulo = ?, autor = ?, ano_publicacao = ? WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, livro.getTitulo());
                statement.setString(2, livro.getAutor());
                statement.setInt(3, livro.getAnoPublicacao());
                statement.setInt(4, livro.getId());
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deletar(int id) {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, SENHA)) {
            String sql = "DELETE FROM livros WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setInt(1, id);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}